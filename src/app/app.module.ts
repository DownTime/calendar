import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';

import { AppComponent } from './app.component';

import { CommonModule } from '@angular/common';
// import { Demos1Component } from './demos1/demos1.component';
import { AppRoutingModule } from './/app-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { Demos2Component } from './demos2/demos2.component';
// import { KitchensinkComponent } from './kitchensink/kitchensink.component';
// import { ClickabledaysComponent } from './clickabledays/clickabledays.component';
import { RefreshingComponent } from './refreshing/refreshing.component';
// import { GroupmonthViewComponent } from './groupmonth-view/groupmonth-view.component';
// import { SumcalendarComponent } from './sumcalendar/sumcalendar.component';


@NgModule({
  declarations: [
    AppComponent,
    // Demos1Component,
    // Demos2Component,
    // KitchensinkComponent,
    // ClickabledaysComponent,
    RefreshingComponent,
    // GroupmonthViewComponent,
    // SumcalendarComponent
  ],
  imports: [
    BrowserModule,
      // BrowserAnimationsModule,
      CalendarModule.forRoot(),
      CommonModule,
      AppRoutingModule,
      NgbModule,
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
