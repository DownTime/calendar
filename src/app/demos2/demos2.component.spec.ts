import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Demos2Component } from './demos2.component';

describe('Demos2Component', () => {
  let component: Demos2Component;
  let fixture: ComponentFixture<Demos2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Demos2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Demos2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
