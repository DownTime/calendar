import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
// import { Demos1Component } from './demos1/demos1.component';
// import { ClickabledaysComponent } from './clickabledays/clickabledays.component';
// import { GroupmonthViewComponent } from './groupmonth-view/groupmonth-view.component';
import { RefreshingComponent } from './refreshing/refreshing.component';
// import { SumcalendarComponent } from './sumcalendar/sumcalendar.component';


const routes: Routes = [
    { path: '', redirectTo: '/Refresh', pathMatch: 'full' },
    // { path: 'Demos1', component: Demos1Component },
    // { path: 'Click', component: ClickabledaysComponent },
    // { path: 'Group', component: GroupmonthViewComponent },
    { path: 'Refresh', component: RefreshingComponent },
    // { path: 'Sumcalendar', component: SumcalendarComponent },
];

@NgModule({
  imports: [
    CommonModule,
      RouterModule.forRoot(routes)
  ],
  declarations: [],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
