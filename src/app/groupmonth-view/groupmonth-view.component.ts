import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';

export const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
  selector: 'app-groupmonth-view',
    changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './groupmonth-view.component.html',
    styleUrls: ['./groupmonth-view.component.css']
})
export class GroupmonthViewComponent {
    view: string = 'month';

    viewDate: Date = new Date();

    events: CalendarEvent[] = [
        {
            title: 'Event 1',
            color: colors.yellow,
            start: new Date(),
            meta: {
                type: 'warning'
            }
        },
        {
            title: 'Event 2',
            color: colors.yellow,
            start: new Date(),
            meta: {
                type: 'warning'
            }
        },
        {
            title: 'Event 3',
            color: colors.blue,
            start: new Date(),
            meta: {
                type: 'info'
            }
        },
        {
            title: 'Event 4',
            color: colors.red,
            start: new Date(),
            meta: {
                type: 'danger'
            }
        },
        {
            title: 'Event 5',
            color: colors.red,
            start: new Date(),
            meta: {
                type: 'danger'
            }
        }
    ];

    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
        body.forEach(cell => {
            const groups: any = {};
            cell.events.forEach((event: CalendarEvent<{ type: string }>) => {
                groups[event.meta.type] = groups[event.meta.type] || [];
                groups[event.meta.type].push(event);
            });
            cell['eventGroups'] = Object.entries(groups);
            console.log (cell['eventGroups']);
        });
    }
}
