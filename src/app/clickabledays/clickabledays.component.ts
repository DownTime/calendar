import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';


@Component({
  selector: 'app-clickabledays',
    changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './clickabledays.component.html',
  styleUrls: ['./clickabledays.component.css']
})
export class ClickabledaysComponent {
    view = 'month';
    viewDate: Date = new Date();
    events: CalendarEvent[] = [];
    clickedDate: Date;

    open () {
      console.log ('clickedDate => ', this.clickedDate);
    }



}
