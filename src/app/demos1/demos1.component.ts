import { Component, OnInit , ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';

@Component({
  selector: 'app-demos1',
    changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './demos1.component.html',
  styleUrls: ['./demos1.component.css']
})
export class Demos1Component implements OnInit {



  constructor() { }

  ngOnInit() {
  }

    viewDate: Date = new Date();

    events: CalendarEvent[] = [
        {
            start: new Date(),
            title: 'An event',
            // color: colors.red
        }
    ];

    activeDayIsOpen: boolean;

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

}
